<?php

declare(strict_types=1);

namespace XOne\Bundle\SmsplanetNotifierBundle\Tests;

use Symfony\Component\Notifier\Test\TransportFactoryTestCase;
use Symfony\Component\Notifier\Transport\TransportFactoryInterface;
use XOne\Bundle\SmsplanetNotifierBundle\SmsplanetTransportFactory;

class SmsplanetTransportFactoryTest extends TransportFactoryTestCase
{
    public function createFactory(): TransportFactoryInterface
    {
        return new SmsplanetTransportFactory();
    }

    public static function supportsProvider(): iterable
    {
        yield [true, 'smsplanet://host'];
        yield [true, 'smsplanet://host?test=1'];
        yield [true, 'smsplanet://host?from=testFrom'];
        yield [true, 'smsplanet://host?from=testFrom&test=1'];
        yield [false, 'somethingElse://host?from=testFrom'];
    }

    public static function createProvider(): iterable
    {
        yield [
            'smsplanet://host',
            'smsplanet://key:password@host',
        ];

        yield [
            'smsplanet://host?from=testFrom',
            'smsplanet://key:password@host?from=testFrom',
        ];

        yield [
            'smsplanet://host?from=testFrom',
            'smsplanet://key:password@host?from=testFrom&test=0',
        ];

        yield [
            'smsplanet://host?from=testFrom',
            'smsplanet://key:password@host?from=testFrom&test=false',
        ];

        yield [
            'smsplanet://host?from=testFrom&test=1',
            'smsplanet://key:password@host?from=testFrom&test=1',
        ];

        yield [
            'smsplanet://host?from=testFrom&test=1',
            'smsplanet://key:password@host?from=testFrom&test=true',
        ];
    }

    public static function incompleteDsnProvider(): iterable
    {
        yield 'missing key or password' => ['smsplanet://test@host'];

        yield 'missing key and password' => ['smsplanet://host'];
    }

    public static function unsupportedSchemeProvider(): iterable
    {
        yield ['somethingElse://token@host'];
    }
}
