<?php

declare(strict_types=1);

namespace XOne\Bundle\SmsplanetNotifierBundle\Tests;

use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\Mailer\Envelope;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\RawMessage;
use Symfony\Component\Notifier\Message\ChatMessage;
use Symfony\Component\Notifier\Message\EmailMessage;
use Symfony\Component\Notifier\Message\PushMessage;
use Symfony\Component\Notifier\Message\SmsMessage;
use Symfony\Component\Notifier\Test\TransportTestCase;
use Symfony\Component\Notifier\Transport\TransportInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use XOne\Bundle\SmsplanetNotifierBundle\SmsplanetTransport;

class SmsplanetTransportTest extends TransportTestCase
{
    public static function createTransport(HttpClientInterface $client = null, string $from = '', bool $test = false): TransportInterface
    {
        $client ??= new MockHttpClient();

        $transport = new SmsplanetTransport('test', 'test', $from, $client);
        $transport->setHost('host');
        $transport->setTest($test);

        return $transport;
    }

    public static function toStringProvider(): iterable
    {
        yield ['smsplanet://host', self::createTransport()];
        yield ['smsplanet://host?test=1', self::createTransport(test: true)];
        yield ['smsplanet://host?from=testFrom', self::createTransport(from: 'testFrom')];
        yield ['smsplanet://host?from=testFrom&test=1', self::createTransport(from: 'testFrom', test: true)];
    }

    public static function supportedMessagesProvider(): iterable
    {
        yield [new SmsMessage('0611223344', 'Foo')];
    }

    public static function unsupportedMessagesProvider(): iterable
    {
        yield [new ChatMessage('Foo')];
        yield [new PushMessage('Foo', 'Bar')];

        $message = new RawMessage('Foo');
        $envelope = new Envelope(new Address('address@example.com'), [new Address('recipient@example.com')]);

        yield [new EmailMessage($message, $envelope)];
    }
}
