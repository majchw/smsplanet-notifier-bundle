# SmsplanetNotifierBundle

Provides [SMSPLANET](https://smsplanet.pl/) integration for Symfony Notifier.

```bash
composer require x-one/smsplanet-notifier-bundle
```

## DSN example

```
SMSPLANET_DSN=smsplanet://KEY:PASSWORD@default?from=FROM&test=TEST
```

where:

- `KEY` is your API key (token)
- `PASSWORD` is your API password
- `TEST` setting this parameter to "1" (default "0") will result in sending message in test mode (message is validated, but not sent)

See your account info at [https://panel.smsplanet.pl/](https://panel.smsplanet.pl/).

## Adding options to a message

You can use the `SmsplanetOptions` class to add [message options](https://smsplanet.pl/doc/slate/index.html#wyslanie-sms-metoda-post-zalecane):

```php
use Symfony\Component\Notifier\Message\SmsMessage;
use Symfony\Component\Notifier\TexterInterface;
use XOne\Bundle\NotifierBundle\SmsplanetOptions;

$sms = new SmsMessage('+4811111111', 'My message');

$options = (new SmsplanetOptions())
    ->date((new DateTime())->modify('+1 hour'))
    ->name('Start systemu')
    ->clearPolish(true)
    ->param1(['Jan', 'Zbigniew', 'Jerzy'])
    ->param2(['Kowalski', 'Nowak', 'Wiśniewski'])
    ->param3(['54-152', '43-190', '60-118'])
    ->param4(['Wrocław', 'Mikołów', 'Poznań'])
    ->companyId('A005')
    ->transactional(true)
;

// Add the custom options to the sms message and send the message
$sms->options($options);

/**
 * @var TexterInterface $texter
 */
$texter->send($sms);
```

## Resources

- [Contributing](docs/contributing.md)
- [SMSPLANET API Documentation](https://smsplanet.pl/doc/slate/index.html#introduction)
