<?php

use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use XOne\Bundle\SmsplanetNotifierBundle\SmsplanetTransportFactory;

return static function (ContainerConfigurator $configurator) {
    $services = $configurator->services();

    $services
        ->set('x_one_smsplanet_notifier.transport_factory', SmsplanetTransportFactory::class)
        ->parent('notifier.transport_factory.abstract')
        ->tag('texter.transport_factory')
    ;
};
