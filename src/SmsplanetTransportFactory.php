<?php

declare(strict_types=1);

namespace XOne\Bundle\SmsplanetNotifierBundle;

use Symfony\Component\Notifier\Exception\UnsupportedSchemeException;
use Symfony\Component\Notifier\Transport\AbstractTransportFactory;
use Symfony\Component\Notifier\Transport\Dsn;
use Symfony\Component\Notifier\Transport\TransportInterface;

class SmsplanetTransportFactory extends AbstractTransportFactory
{
    public const SCHEME = 'smsplanet';

    public function create(Dsn $dsn): TransportInterface
    {
        $scheme = $dsn->getScheme();

        if (self::SCHEME !== $scheme) {
            throw new UnsupportedSchemeException($dsn, self::SCHEME, $this->getSupportedSchemes());
        }

        $key = $this->getUser($dsn);
        $password = $this->getPassword($dsn);
        $from = $dsn->getOption('from', '');
        $host = 'default' === $dsn->getHost() ? null : $dsn->getHost();
        $test = filter_var($dsn->getOption('test', false), \FILTER_VALIDATE_BOOL);
        $port = $dsn->getPort();

        return (new SmsplanetTransport($key, $password, $from, $this->client, $this->dispatcher))
            ->setHost($host)
            ->setPort($port)
            ->setTest($test)
        ;
    }

    protected function getSupportedSchemes(): array
    {
        return [self::SCHEME];
    }
}
