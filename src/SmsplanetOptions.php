<?php

declare(strict_types=1);

namespace XOne\Bundle\SmsplanetNotifierBundle;

use Symfony\Component\Notifier\Message\MessageOptionsInterface;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SmsplanetOptions implements MessageOptionsInterface
{
    private const DATE_FORMAT = 'd-m-Y H:i:s';

    private array $resolvedOptions = [];
    private OptionsResolver $optionsResolver;
    private bool $dirty = true;

    public function __construct(
        private array $options = [],
    ) {
    }

    public function toArray(): array
    {
        if ($this->dirty) {
            $this->resolvedOptions = array_filter($this->getOptionsResolver()->resolve($this->options));
            $this->dirty = false;
        }

        return $this->resolvedOptions;
    }

    public function getRecipientId(): ?string
    {
        return null;
    }

    public function date(null|\DateTimeInterface|string $date): static
    {
        if ($date instanceof \DateTimeInterface) {
            $date = $date->format(static::DATE_FORMAT);
        }

        return $this->setOption('date', $date);
    }

    public function name(?string $name): static
    {
        return $this->setOption('name', $name);
    }

    public function clearPolish(?bool $clearPolish): static
    {
        return $this->setOption('clear_polish', $clearPolish);
    }

    public function param1(null|string|array $param1): static
    {
        return $this->setOption('param1', $this->formatParam($param1));
    }

    public function param2(null|string|array $param2): static
    {
        return $this->setOption('param2', $this->formatParam($param2));
    }

    public function param3(null|string|array $param3): static
    {
        return $this->setOption('param3', $this->formatParam($param3));
    }

    public function param4(null|string|array $param4): static
    {
        return $this->setOption('param4', $this->formatParam($param4));
    }

    public function companyId(?string $companyId): static
    {
        return $this->setOption('company_id', $companyId);
    }

    public function transactional(?bool $transactional): static
    {
        return $this->setOption('transactional', $transactional);
    }

    private function setOption(string $key, mixed $value): static
    {
        $this->dirty = true;

        if (null === $value) {
            unset($this->options[$key]);
        }

        $this->options[$key] = $value;

        return $this;
    }

    private function formatParam(null|string|array $param): ?string
    {
        if (is_array($param)) {
            return implode('|', array_filter($param));
        }

        return $param;
    }

    private function getOptionsResolver(): OptionsResolver
    {
        return $this->optionsResolver ??= (new OptionsResolver())
            ->setDefaults([
                'date' => null,
                'name' => null,
                'clear_polish' => null,
                'param1' => null,
                'param2' => null,
                'param3' => null,
                'param4' => null,
                'company_id' => null,
                'transactional' => null,
            ])
            ->setAllowedTypes('date', ['null', 'string'])
            ->setAllowedTypes('name', ['null', 'string'])
            ->setAllowedTypes('clear_polish', ['null', 'bool', 'int'])
            ->setAllowedTypes('param1', ['null', 'string'])
            ->setAllowedTypes('param2', ['null', 'string'])
            ->setAllowedTypes('param3', ['null', 'string'])
            ->setAllowedTypes('param4', ['null', 'string'])
            ->setAllowedTypes('company_id', ['null', 'string'])
            ->setAllowedTypes('transactional', ['null', 'bool', 'int'])
            ->setNormalizer('clear_polish', fn (Options $options, null|bool|int $value) => $this->normalizeBooleanValue($value))
            ->setNormalizer('transactional', fn (Options $options, null|bool|int $value) => $this->normalizeBooleanValue($value))
        ;
    }

    private function normalizeBooleanValue(null|bool|int $value): ?int
    {
        if (null === $value) {
            return null;
        }

        return $value ? 1 : 0;
    }
}
