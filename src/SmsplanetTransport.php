<?php

declare(strict_types=1);

namespace XOne\Bundle\SmsplanetNotifierBundle;

use Symfony\Component\Notifier\Exception\InvalidArgumentException;
use Symfony\Component\Notifier\Exception\TransportException;
use Symfony\Component\Notifier\Exception\UnsupportedMessageTypeException;
use Symfony\Component\Notifier\Message\MessageInterface;
use Symfony\Component\Notifier\Message\MessageOptionsInterface;
use Symfony\Component\Notifier\Message\SentMessage;
use Symfony\Component\Notifier\Message\SmsMessage;
use Symfony\Component\Notifier\Transport\AbstractTransport;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\HttpExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class SmsplanetTransport extends AbstractTransport
{
    protected const HOST = 'api2.smsplanet.pl';

    public bool $test = false;

    public function __construct(
        #[\SensitiveParameter] private readonly string $key,
        #[\SensitiveParameter] private readonly string $password,
        private readonly string $from = '',
        HttpClientInterface $client = null,
        EventDispatcherInterface $dispatcher = null,
    ) {
        parent::__construct($client, $dispatcher);
    }

    public function setTest(bool $test): static
    {
        $this->test = $test;

        return $this;
    }

    /**
     * @throws TransportExceptionInterface
     * @throws HttpExceptionInterface
     */
    protected function doSend(MessageInterface $message): SentMessage
    {
        if (!$message instanceof SmsMessage) {
            throw new UnsupportedMessageTypeException(__CLASS__, SmsMessage::class, $message);
        }

        $endpoint = sprintf('https://%s/sms', $this->getEndpoint());

        $response = $this->client->request('POST', $endpoint, [
            'body' => $this->getRequestBody($message),
        ]);

        try {
            $statusCode = $response->getStatusCode();
        } catch (TransportExceptionInterface $exception) {
            throw new TransportException('Could not reach the remote SMSPLANET server.', $response, previous: $exception);
        }

        try {
            $content = $response->toArray(false);
        } catch (DecodingExceptionInterface $exception) {
            throw new TransportException('Could not decode body to an array.', $response, previous: $exception);
        }

        if (200 !== $statusCode) {
            throw new TransportException(sprintf('Unable to send the SMS: the response status code was %s.', $statusCode), $response);
        }

        $errorMessage = $content['errorMsg'] ?? null;
        $errorCode = $content['errorCode'] ?? null;

        if (null !== $errorMessage || null !== $errorCode) {
            $errorCode ??= 'unknown';
            $errorMessage ??= 'unknown error';

            throw new TransportException(sprintf('Unable to send the SMS: status code "%s", message "%s".', $errorCode, $errorMessage), $response);
        }

        $message = new SentMessage($message, (string) $this);

        if (!empty($messageId = $content['messageId'] ?? null)) {
            $message->setMessageId((string) $messageId);
        }

        return $message;
    }

    public function supports(MessageInterface $message): bool
    {
        return $message instanceof SmsMessage;
    }

    public function __toString(): string
    {
        $dsn = sprintf('%s://%s', SmsplanetTransportFactory::SCHEME, $this->getEndpoint());

        $queryParameters = array_filter([
            'from' => $this->from,
            'test' => $this->test,
        ]);

        if (!empty($queryParameters)) {
            $dsn .= '?'.http_build_query($queryParameters, arg_separator: '&');
        }

        return $dsn;
    }

    private function getRequestBody(SmsMessage $message): array
    {
        $from = $message->getFrom() ?: $this->from;

        if (empty($from)) {
            throw new InvalidArgumentException('The message "from" has to be provided as the SMSPLANET_DSN does not have "from" parameter set.');
        }

        $body = [
            'key' => $this->key,
            'password' => $this->password,
            'from' => $message->getFrom() ?: $this->from,
            'to' => $message->getPhone(),
            'msg' => $message->getSubject(),
            'test' => $this->test,
        ];

        $options = $message->getOptions();

        if (null !== $options) {
            if (false === $options instanceof SmsplanetOptions) {
                $options = new SmsplanetOptions($options->toArray());
            }

            foreach ($options->toArray() as $key => $value) {
                if (null !== $value) {
                    $body[$key] = $value;
                }
            }
        }

        return $body;
    }
}
