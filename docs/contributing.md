# Contributing

## Tests

Please make sure, that all tests are passing:

```bash
vendor/bin/simple-phpunit
```

## Quality control

Please make sure, that the code is properly formatted, and the PHPStan is not reporting any errors:

```bash
vendor/bin/php-cs-fixer fix
vendor/bin/phpstan analyse
```

## Versioning

To allow updating the package through the Composer, after merging the code to the master branch, create a new tag in `vX.Y.Z` format:

```bash
git tag -a v1.1.0 -m "Version v1.1.0"
git push origin --tags
```
